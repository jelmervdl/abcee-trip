<?php

date_default_timezone_set('Europe/Amsterdam');

$fields = [
	'name',
	'address',
	'city',
	'email',
	'phone_number',
	'iban',
	'remarks',
	'accept_terms',
	'time'
];

$_POST['time'] = date('r');

$data = array_combine($fields, array_map(function($field) {
	return isset($_POST[$field]) ? $_POST[$field] : null;
}, $fields));

if (!$data['accept_terms'])
	die("You've got to accept the terms...");

if (filter_var($data['email'], FILTER_VALIDATE_EMAIL) === false)
	die("PHP does not accept your email address. Sorry about that. Could you contact abcee@svcover.nl directly instead?");

$email = <<<EOF
Hey,

You've signed up for the ABCee trip to Cologne, so you've got that going for you which is nice!

As a reminder, you signed up as the following person:

	Name: $data[name]
	Address: $data[address]
	City: $data[city]

	Email: $data[email]
	Phone: $data[phone_number]

	IBAN: $data[iban]

	Additional remarks:
	$data[remarks]

	You signed up at $data[time] and you apparently accepted the terms!

Well, great. We'll be in touch.

Kind regards,

The ABCee
EOF;

if (mail(
	sprintf('ABCee <abcee@svcover.nl>, %s <%s>', $data['name'], $data['email']),
	'Sign-up Cologne',
	$email)) {
	echo "Great! You should receive a confirmation email shortly!";
	echo '<br><a href="/">Back to the website!</a>';
}


if (($fh = fopen('signups.txt', 'a')) !== false) {
	fputcsv($fh, array_values($data));
	fclose($fh);
}
